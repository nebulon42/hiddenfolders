# Hidden Folders

A simple extension for Thunderbird to hide some IMAP folders you cannot
unsubscribe from. Simply set the "Hide folder" flag on the folders you don't
want to show up in your folder list and switch to the "Visible Folders" view
mode for the settings to take effect.

Useful with mail services that use IMAP folders for storing calendar, contact
or other data such as [Kolab](https://www.kolab.org) or
[KolabNow](https://www.kolabnow.com).

## Download

Grab the XPI from the GitHub release page.

## Releases

0.1.0 - initial release

## Credits

Heavily inspired by the [Folder Levels](https://github.com/jeremyiverson/folderlevels) add-on.
