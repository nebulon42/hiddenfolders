function initFolderPaneContextMenu(event) {
    var folders = gFolderTreeView.getSelectedFolders();
    if (!folders.length)
        return false;

    var numSelected = folders.length;

    ShowMenuItem("folderPaneContext-hiddenFolder", numSelected == 1
        && !folders[0].isServer && folders[0].server.type == "imap");

    var hidden = folders[0].getStringProperty("hiddenFolder");
    if (hidden === "hidden") {
        document.getElementById("folderPaneContext-hiddenFolder").setAttribute("checked", true);
    }
    else {
        document.getElementById("folderPaneContext-hiddenFolder").setAttribute("checked", false);
    }

    return fillFolderPaneContextMenu(event) && true;
}

function toggleHiddenFolderFlag() {
    var folder = GetFirstSelectedMsgFolder();
    var prop = "hiddenFolder";
    var hidden = folder.getStringProperty(prop);

    if (hidden === "visible") {
        folder.setStringProperty(prop, "hidden");
    }
    else if (hidden === "hidden") {
        folder.setStringProperty(prop, "visible");
    }
    else {
        folder.setStringProperty(prop, "hidden");
    }

    gFolderTreeView._rebuild();
}

function setHideFoldersViewMode() {
    gFolderTreeView.mode = "hiddenfolders";
}

var hiddenfoldersMode = {
    __proto__: IFolderTreeMode,

    _isVisible: function hidefolder_isVisible(folder) {
        if (folder) {
            var hidden = folder.getStringProperty("hiddenFolder");
            if (hidden === "hidden") {
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    },

    generateMap: function hiddenfolders_generateMap(ftv) {
        var filterHidden = function filterHidden(aFolder) {
            return this._isVisible(aFolder);
        };
        var accounts = gFolderTreeView._sortedAccounts();
        // Force each root folder to do its local subfolder discovery.
        MailUtils.discoverFolders();

        var map = [];

        for (var acct of accounts) {
            var rootFolder = acct.incomingServer.rootFolder;
            map.push(new ftvItem(rootFolder, this._isVisible));
        }

        return map;
    }
};

gFolderTreeView.registerFolderTreeMode("hiddenfolders", hiddenfoldersMode, "Visible Folders");
